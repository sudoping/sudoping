const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
const path = require('path');

const app = express();
const port = 3000;

// Fonction pour exporter les données vers JSON
function exportToJson(callback) {
    // Connexion à la base SQLite
    const db = new sqlite3.Database('./test.sqlite');

    db.all(
        `SELECT joueurs.id, joueurs.nom, joueurs.tags, joueurs.image, joueurs.age, joueurs.taille, joueurs.popularite, joueurs.ldc, joueurs.cdm, joueurs.ballon_dor,
                club.nom AS equipe, club.logo AS club_logo,
                pays.nom AS pays, pays.drapeau AS pays_drapeau
         FROM joueurs
         LEFT JOIN club ON joueurs.equipe_id = club.id
         LEFT JOIN pays ON joueurs.pays_id = pays.id`,
        (err, rows) => {
            if (err) {
                console.error('Erreur lors de la récupération des données:', err.message);
                callback(err);
                return;
            }

            // Traiter les données pour correspondre au format souhaité
            const players = rows.map(row => ({
                "id": row.id,
                "nom": row.nom,
                "img": row.image,
                "tags": row.tags ? row.tags.split(',').map(tag => tag.trim()) : [],
                "pays": row.pays,
                "pays_img": row.pays_drapeau,    // Ajout de l'image du pays
                "clubs": row.equipe,
                "club_img": row.club_logo,       // Ajout de l'image du club
                "age": row.age,
                "taille": row.taille,
                "popularite": row.popularite,
                "ldc": row.ldc,
                "cdm": row.cdm,
                "ballon_dor": row.ballon_dor,
            }));

            const data = {
                "player": players
            };

            // Chemin vers le fichier 'data.json' dans le dossier 'public'
            const dataPath = path.join(__dirname, 'public', 'data.json');

            // Exporter les données dans un fichier JSON dans le dossier 'public'
            fs.writeFile(dataPath, JSON.stringify(data, null, 2), (err) => {
                if (err) {
                    console.error('Erreur lors de l\'écriture du fichier data.json:', err);
                    callback(err);
                    return;
                }
                console.log('Données exportées vers', dataPath);
                callback(null);
            });
        }
    );

    db.close();
}


// Appeler la fonction exportToJson au démarrage du serveur
exportToJson((err) => {
    if (err) {
        console.error('Erreur lors de l\'exportation des données:', err);
    } else {
        console.log('Exportation des données réussie.');
    }
});

app.use(express.json());
app.use(express.static('public')); // Pour servir les fichiers statiques (HTML, JS, CSS)

app.get('/search', (req, res) => {
    const keyword = req.query.keyword?.toLowerCase() || '';  // Assurez-vous que la recherche est insensible à la casse

    // Marquer le temps de début
    const startTime = process.hrtime(); // Utilisation de process.hrtime pour une meilleure précision

    // Recherche dans les données
    const results = playersData.filter(player => {
        return (
            player.nom?.toLowerCase().includes(keyword) ||
            player.equipe?.toLowerCase().includes(keyword) ||
            player.tags?.toLowerCase().includes(keyword)
        );
    });

    // Trier par popularité décroissante
    results.sort((a, b) => b.popularite - a.popularite);

    // Marquer le temps de fin
    const [seconds, nanoseconds] = process.hrtime(startTime); // Temps écoulé en nanosecondes
    const executionTime = (seconds * 1000) + (nanoseconds / 1000000); // Convertir en millisecondes

    // Log dans un fichier 'temps_log.txt'
    const logMessage = `Recherche avec le mot-clé "${keyword}" a pris ${executionTime.toFixed(3)}ms\n`;

    // Append du message dans le fichier log
    fs.appendFile('temps_log.txt', logMessage, (err) => {
        if (err) {
            console.error('Erreur d\'écriture dans le fichier log:', err);
        }
    });

    // Renvoyer les résultats
    res.json(results);
});

// Démarrer le serveur
app.listen(port, () => {
    console.log(`Serveur en écoute sur http://localhost:${port}`);
});
