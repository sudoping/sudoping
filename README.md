# Lucarne Web
> Version Finale

# SudoPing

# SAE Développement avancé

## Présentations
Nous sommes des étudians du groupe de la classe Géryon, composé de Deniz YUKER, Ahmed EL SAADAWY, Azzedine HATEM, Tristan COLDEFY et Slimane AINOUZ.

## Comment lancer l'application
Installer NodeJs<br>
Installer les dépendances : npm install<br>
Lancer le serveur avec "node app.js"

## Algorithme - Distance de Levenshtein 

Mesure de similarité entre deux chaînes de caractères selon 3 opérations : 

Insertion : Ajouter un caractère.
Suppression : Retirer un caractère.
Substitution : Remplacer un caractère par un autre.

Chaque opération coute 1 unité.

Exemple : Chaîne 1 : "Ronald" et Chaîne 2 : "Ronaldo" Donc on cherche Ronald mais on voulait voir apparaitre Ronaldo
Pour transformer Ronald" en "Ronaldo", nous devons :
Ajouter "o" (insertion). Le nombre d’opérations nécessaires est 1, donc la distance de Levenshtein entre "Ronald" et "Ronaldo" est 1.

On peut ajuster dans notre code le seuil de tolérance de Levenshtein pour avoir des résultats précis selon les tags ou plus larges
