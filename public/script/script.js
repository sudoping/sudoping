"use strict";
let players = [];
fetch('data.json')
    .then(blob => blob.json())
    .then(data => {
    players.push(...data.player);
});

function distanceLevenshtein(a, b) {
    a = a.toLowerCase();
    b = b.toLowerCase();
    const m = a.length;
    const n = b.length;

    if (m === 0) return n;
    if (n === 0) return m;

    const dp = Array.from({ length: m + 1 }, () => Array(n + 1).fill(0));

    for (let i = 0; i <= m; i++) dp[i][0] = i;
    for (let j = 0; j <= n; j++) dp[0][j] = j;

    for (let i = 1; i <= m; i++) {
        for (let j = 1; j <= n; j++) {
            const cost = a[i - 1] === b[j - 1] ? 0 : 1;
            dp[i][j] = Math.min(
                dp[i - 1][j] + 1,  // Suppression
                dp[i][j - 1] + 1,  // Insertion
                dp[i - 1][j - 1] + cost // Substitution
            );
        }
    }

    return dp[m][n];
}


function trouverJoueur(recherche, players) {
    const rechercheLower = recherche.toLowerCase(); // Recherche insensible à la casse
    const seuilDistance = Math.max(1, Math.floor(recherche.length / 4)); // Tolérance plus stricte


    return players
        .map(player => {
            const champs = [
                player.nom.toLowerCase(),
                ...player.tags.map(tag => tag.toLowerCase()),
                player.pays.toLowerCase(),
                player.clubs.toLowerCase()
            ];

            const sousChaineMatch = champs.some(champ => champ.includes(rechercheLower));

            // Si correspondance par sous-chaîne, donner une distance très basse (priorité absolue)
            if (sousChaineMatch) {
                return { player, distance: -1 }; // Priorité maximale
            }

            const distances = champs.map(champ => distanceLevenshtein(rechercheLower, champ));
            const minDistance = Math.min(...distances);

            return { player, distance: minDistance };
        })
        .filter(({ distance }) => distance <= seuilDistance || distance === -1) // Inclure les sous-chaînes
        .sort((a, b) => a.distance - b.distance || b.player.popularite - a.player.popularite)
        .map(({ player }) => player);

}



function afficherResultat() {
    if (input.value.length >= 1) {
        const tabResult = trouverJoueur(input.value, players);
        const html = tabResult.map(player => {
            return `
                <li class="d-flex align-items-center justify-content-between" style="height: 70px; padding: 0 10px;">
                    <div class="d-flex align-items-center text-decoration-none text-dark w-100" style="height: 70px;">
                        <div class="d-flex align-items-center" style="flex-grow: 1;">
                            <img src='${player.pays_img}' alt='${player.pays}' style="width: 50px; height: 70px; object-fit: contain; filter: invert(0); margin-right: 10px;"/>
                            <img src='${player.club_img}' alt='${player.clubs}' style="width: 50px; height: 70px; object-fit: cover; filter: invert(0); margin-right: 10px;"/>
                            <img src='${player.img}' alt='${player.nom}' style="width: 50px; height: 70px; object-fit: cover; filter: invert(0); margin-right: 10px;"/>
                            <span class="d-block overflow-hidden text-nowrap text-truncate">${player.nom}</span>
                        </div>
                        <div style="text-align: right;">
                            <span>${player.popularite}</span>
                        </div>
                    </div>
                </li>
            `;
        }).join('');
        result.innerHTML = html;
    }
    else {
        result.innerHTML = "";
    }
}
const input = document.querySelector('#search-input');
const result = document.querySelector('#result-list');
const form = document.getElementById('formulaire');
form.addEventListener('submit', function (e) {
    e.preventDefault();
});
input.addEventListener('input', afficherResultat);


