let players: any[] = [];

fetch('data.json')
    .then(blob => blob.json())
    .then(data => {
        players.push(...data.player);
    });

function trouverJoueur(recherche: string, players: any[]) {
    const regex = new RegExp(recherche, 'gi');
    return players
        .filter(player => {
            return player.nom.match(regex) ||
                player.tags.find((tag: string) => tag.match(regex)) ||
                player.pays.match(regex) ||
                player.clubs.match(regex);
        })
        .sort((a, b) => b.popularite - a.popularite);
}


function afficherResultat() {
    if (input.value.length >= 1) {
        const tabResult = trouverJoueur(input.value, players);
        const html = tabResult.map(player => {
            return `
                <li class="d-flex align-items-center justify-content-between" style="height: 70px; padding: 0 10px;">
                    <a href="player.html?id=${player.id}" class="d-flex align-items-center text-decoration-none text-dark w-100" style="height: 70px;">
                        <div class="d-flex align-items-center" style="flex-grow: 1;">
                            <img src='${player.pays_img}' alt='${player.pays}' style="width: 50px; height: 70px; object-fit: contain; filter: invert(0); margin-right: 10px;"/>
                            <img src='${player.club_img}' alt='${player.clubs}' style="width: 50px; height: 70px; object-fit: cover; filter: invert(0); margin-right: 10px;"/>
                            <img src='${player.img}' alt='${player.nom}' style="width: 50px; height: 70px; object-fit: cover; filter: invert(0); margin-right: 10px;"/>
                            <span class="d-block overflow-hidden text-nowrap text-truncate">${player.nom}</span>
                        </div>
                        <div style="text-align: right;">
                            <span>${player.popularite}</span>
                        </div>
                    </a>
                </li>
            `;
        }).join('');
        result.innerHTML = html;
    } else {
        result.innerHTML = "";
    }
}

const input = document.querySelector('#search-input') as HTMLInputElement;
const result = document.querySelector('#result-list')!;

const form = document.getElementById('formulaire') as HTMLFormElement;
form.addEventListener('submit', function (e) {
    e.preventDefault();
});

input.addEventListener('input', afficherResultat);
