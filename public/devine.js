const hintsList = document.getElementById('hints-list');
const guessInput = document.getElementById('guess-input');
const submitGuessButton = document.getElementById('submit-guess');
const resultElement = document.getElementById('result');

let playerData = [];
let currentPlayer = null;

// Charger les données des joueurs
fetch('/data.json')
    .then(response => response.json())
    .then(data => {
        playerData = data.player;
        startGame();
    })
    .catch(error => console.error('Erreur lors du chargement des données:', error));

function startGame() {
    // Sélectionner un joueur aléatoire
    currentPlayer = playerData[Math.floor(Math.random() * playerData.length)];
    
    // Afficher les indices
    showHints();
}

function showHints() {
    hintsList.innerHTML = '';

    // Indice 1 : Tags
    const tagHint = document.createElement('li');
    tagHint.textContent = `Tags : ${currentPlayer.tags.join(', ')}`;
    hintsList.appendChild(tagHint);

    // Indice 2 : Nombre de lettres dans le nom
    const nameHint = document.createElement('li');
    nameHint.textContent = `Nombre de lettres dans le nom : ${currentPlayer.nom.length}`;
    hintsList.appendChild(nameHint);

    // Indice 3 : Première lettre
    const firstLetterHint = document.createElement('li');
    firstLetterHint.textContent = `Première lettre : ${currentPlayer.nom[0]}`;
    hintsList.appendChild(firstLetterHint);
}


// Vérifier la réponse
submitGuessButton.addEventListener('click', () => {
    const userGuess = guessInput.value.trim();

    if (userGuess.toLowerCase() === currentPlayer.nom.toLowerCase()) {
        resultElement.textContent = `🎉 Bravo ! Vous avez trouvé : ${currentPlayer.nom}`;
        resultElement.style.color = 'green';
        submitGuessButton.textContent = 'Rejouer';
        submitGuessButton.onclick = startGame;
    } else {
        resultElement.textContent = `❌ Incorrect ! Essayez encore.`;
        resultElement.style.color = 'red';
    }
});