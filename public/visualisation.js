const countrySelect = document.getElementById('country-select');
const positionSelect = document.getElementById('position-select');
const ageSelect = document.getElementById('age-select');
const applyFiltersButton = document.getElementById('apply-filters');
const playerChartCanvas = document.getElementById('playerChart');

let playerData = [];
let chartInstance = null;

// Charger les données des joueurs
fetch('/data.json')
    .then(response => response.json())
    .then(data => {
        playerData = data.player;

        populateFilters();
        updateChart(playerData);
    })
    .catch(error => console.error('Erreur lors du chargement des données:', error));

// Remplir les options des filtres
function populateFilters() {
    const countries = new Set(playerData.map(player => player.pays));
    const positions = new Set(playerData.flatMap(player => player.tags));

    countries.forEach(country => {
        const option = document.createElement('option');
        option.value = country;
        option.textContent = country;
        countrySelect.appendChild(option);
    });

    positions.forEach(position => {
        const option = document.createElement('option');
        option.value = position;
        option.textContent = position;
        positionSelect.appendChild(option);
    });
}

// Appliquer les filtres
applyFiltersButton.addEventListener('click', () => {
    const filteredData = applyFilters();
    updateChart(filteredData);
});

function applyFilters() {
    const selectedCountry = countrySelect.value;
    const selectedPosition = positionSelect.value;
    const selectedAge = ageSelect.value;

    return playerData.filter(player => {
        const age = player.age;
        const matchesCountry = !selectedCountry || player.pays === selectedCountry;
        const matchesPosition = !selectedPosition || player.tags.includes(selectedPosition);
        const matchesAge = !selectedAge ||
            (selectedAge === 'under20' && age < 20) ||
            (selectedAge === '20to29' && age >= 20 && age <= 29) ||
            (selectedAge === '30plus' && age >= 30);

        return matchesCountry && matchesPosition && matchesAge;
    });
}

// Mettre à jour le graphique
function updateChart(data) {
    const countries = data.map(player => player.pays);
    const countryCounts = countries.reduce((acc, country) => {
        acc[country] = (acc[country] || 0) + 1;
        return acc;
    }, {});

    const labels = Object.keys(countryCounts);
    const values = Object.values(countryCounts);

    if (chartInstance) {
        chartInstance.destroy();
    }

    chartInstance = new Chart(playerChartCanvas, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: 'Nombre de joueurs par pays',
                data: values,
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false, // Permet un ajustement flexible
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    
}
